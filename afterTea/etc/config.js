/**
 *
 *  config.js which contains the configuration of app, and it should never be cached
 *
 *  @author  lihao
 *  @date    Oct 31, 2016
 *
 **/

export default {appname: 'afterTea', version: '1.0.0'};
