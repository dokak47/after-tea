/**
 *  Entrance of features
 *
 *  @author  lihao
 *  @date    Oct 31, 2016
 *
 */

import common from './common/main';
import home from './home/main';
import about from './about/main';

export default [about, ...common, home];
