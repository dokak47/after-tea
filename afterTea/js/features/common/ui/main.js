/**
 *  Entrance of common ui
 *
 *  @author  lihao
 *  @date    Oct 31, 2016
 *
 */

import autofocus from './Autofocus';
import routeIndicator from './RouteIndicator';

export default [autofocus, routeIndicator];
