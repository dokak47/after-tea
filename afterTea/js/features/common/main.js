/**
 *  Entrance of common service
 *
 *
 *  @author  lihao
 *  @date    Oct 31, 2016
 *
 */

import logical from './logical/main';
import ui from './ui/main';

export default [...logical, ...ui];
