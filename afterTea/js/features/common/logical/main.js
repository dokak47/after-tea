/**
 *  Entrance of common logical
 *
 *  @author  lihao
 *  @date    Oct 31, 2016
 *
 */

import header from './Header';

export default [header];
