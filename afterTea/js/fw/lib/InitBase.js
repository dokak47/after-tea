/**
 *  InitBase class
 *
 *
 *  @author  lihao
 *  @date    Oct 31, 2016
 *
 */

class InitBase {

    constructor(features) {
        this.features = features;
    }

    execute() {}
}

export default InitBase;
