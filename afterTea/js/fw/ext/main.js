/**
 *  Return all information of external angular resources
 *
 *  @author  lihao
 *  @date    Oct 31, 2016
 *
 */

import 'angular-route';

export default ['ngRoute'];
