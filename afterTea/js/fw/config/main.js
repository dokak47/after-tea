/**
 *  Entrance of config
 *
 *
 *  @author  lihao
 *  @date    Oct 31, 2016
 *
 */

import router from './RouterConfig';
import sso from './SSOConfig';

export default [router, sso];
